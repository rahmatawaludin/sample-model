<?php namespace App\Scope;

trait Published {

    /**
     * Boot the published trait for a model.
     *
     * @return void
     */
    public static function bootPublished()
    {
        static::addGlobalScope(new PublishedScope);
    }

    public static function withDrafts()
    {
        return with(new static)->newQueryWithoutScope(new PublishedScope);
    }

}
