<?php namespace App\Scope;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ScopeInterface;

class PublishedScope implements ScopeInterface {

	/**
     * Apply scope on the query.
     *
     * @param \Illuminate\Database\Eloquent\Builder  $builder
     * @param \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
	public function apply(Builder $builder, Model $model)
	{
		$builder->where('published', 1);
	}

	public function remove(Builder $builder, Model $model)
	{
		$query = $builder->getQuery();

	    foreach ((array) $query->wheres as $key => $where)
	    {
	    	// check if this where is published constraint
	    	if ($where['type'] == 'Basic' && $where['column'] == 'published' && $where['value'] == 1) {
	    		// remove from query options
	    		unset($query->wheres[$key]);
	    		$query->wheres = array_values($query->wheres);
	    		// remove from binding
	    		$bindings = $query->getRawBindings()['where'];
		        unset($bindings[$key]);
		        $query->setBindings($bindings);
	    	}
	    }
	    return $query;
	}


}
