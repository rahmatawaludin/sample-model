<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model {
	use \App\Scope\PublishedTrait;
	use SoftDeletes;

	protected $dates = ['deleted_at'];
	public $timestamps = false;
	protected $visible = ['name', 'price', 'stock'];
	protected $fillable = ['name', 'description', 'price', 'stock'];
	protected $casts = [
	    'price' => 'double'
	];

	protected static function boot()
	{
		parent::boot();

		static::observe(new \App\ProductObserver);
	}


	public function scopeOverstock($query)
	{
		return $query->where('stock', '>', 30);
	}

	public function scopeOverprice($query)
	{
		return $query->where('price', '>', 400000000);
	}

	public function scopePremium($query)
	{
		return $query->overstock()->overprice();
	}

	public function scopeLevel($query, $parameter)
	{
		switch ($parameter) {
			case 'lux':
				return $query->where('price', '>', 500000000);
				break;
			case 'mid':
				return $query->whereBetween('price', [300000000,500000000]);
				break;
			case 'entry':
				return $query->where('price', '<', 300000000);
				break;
			default:
				return $query;
				break;
		}
	}
}
