<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::resource('home', 'HomeController');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::get('/customer/{id}', function($id) {
	try {
		$customer = App\Models\Customer::findOrFail($id);
		return $customer;
	} catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
		return "Oppsss.. Customer tidak ditemukan";
	}
});

Route::get('/product/{id}', function($id) {
	return App\Product::find($id);
});

Route::get('/products', function() {
	return App\Product::all();
});

Route::get('/test', function() {
	$query = App\Post::where('id', 1);
	$query->where('published', 1);
	unset($query->getQuery()->wheres[1]);
	$bindings = $query->getQuery()->getRawBindings()['where'];
	unset($bindings[1]);
	$query->getQuery()->setBindings($bindings);
	return $query->get();
});

