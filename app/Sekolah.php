<?php namespace App;

class Sekolah {
	protected $nama;
	protected $alamat;

	public function __construct($nama, $alamat)
	{
		$this->nama = $nama;
		$this->alamat = $alamat;
	}

	public function __toString()
	{
		return json_encode(['nama' => $this->nama, 'alamat' => $this->alamat]);
	}
}
